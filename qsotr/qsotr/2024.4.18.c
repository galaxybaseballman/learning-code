#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int  cmp_int(const void* p1, const void* p2)
{
	return strcmp(*(char**)p2,*(char**)p1);
}
void swap(char* pe1,char* pe2,size_t width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *pe1;
		*pe1 = *pe2;
		*pe2 = tmp;
		pe1++;
		pe2++;
	}
}
void diao(void* base,size_t sz,size_t width,int (*cmp)(const void* p1, const void* p2) )
{
	for (int i = 0; i < sz - 1; i++)
	{
		for (int j = 0; j < sz - i - 1; j++)
		{
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}

}
int main()
{
	char* arr[4] = { "cui","ming","xue" ,"abcd"};//**arr = *arr[0] = 'c'
	int sz = sizeof(arr) / sizeof(arr[0]);
	diao(arr, sz, sizeof(arr[0]), cmp_int);
	for (int i = 0; i < sz; i++)
	{
		printf("%s ", arr[i]);
	}
	return 0;
}
