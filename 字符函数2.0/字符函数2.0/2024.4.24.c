#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
void my_strcpy(char* dest, const char* src)
{
	int ret = dest;
	while (*src != '\0')
	{
		*dest = *src;
		*dest++;
		*src++;
	}
	return ret;
}
int main()
{
	char arr[20] = "cuiming";
	char arr1[20] = "xue";
	my_strcpy(arr1, arr);
	printf("%s", arr1);
	return 0;
}