#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

void InsertSort(int* arr, int n)
{
	int i = 1;

	while(i < n)
	{
		int tmp = arr[i];
		int j = i;
		while (j--)
		{
			if (tmp < arr[j])
			{
				arr[j + 1] = arr[j];
			}
			else
				break;
		}
		arr[j + 1] = tmp;
		i++;
	}
}

int main()
{
	int a[] = { 3,7,3,789,3,1,74,4523,734,42,89,357,43,66 };
	InsertSort(a, sizeof(a) / sizeof(a[0]));
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); i++)
		printf("%d ", a[i]);
	return 0;
}