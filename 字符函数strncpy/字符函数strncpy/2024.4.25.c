#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<assert.h>
int  my_strncpy(char* arr, const char* arr1, size_t num)
{
	assert(arr && arr1);
	while (num--)
	{
		*arr++ = *arr1++;
		if (arr1 == '\0')
		{
			return 0;
		}
	}

}
int main()
{
	char arr[20] = "cui";
	char arr1[20] = "mingxue";
	my_strncpy(arr, arr1,9);
	printf("%s", arr);
	return 0;
}