#define _CRT_SECURE_NO_WARNINGS 1
#include"mystring.h"

//size_t mystrlen(const char* str)
//{
//	int a = 0;
//
//	while (str[a] != '\0')
//	{
//		a++;
//	}
//	return a;
//}

size_t mystrlen(const char* str)
{
	const char* tmp = str;
	while (*tmp != '\0')
	{
		tmp++;
	}
	return tmp - str;
}