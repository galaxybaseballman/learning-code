#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int a, b;
    while (scanf("%d%d", &a, &b) == 2) {
        if (a == b)
            printf("%d=%d\n", a, b);
        if (a > b)
            printf("%d>%d\n", a, b);
        if (a < b)
            printf("%d<%d\n", a, b);
    }
    return 0;
}