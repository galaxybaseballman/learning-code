#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int NumberOf1(int n) {
   
    int sum = 0;
    for (int i = 0; i < 32; i++)
    {
        if (n & (1 << i))
            sum++;
    }
    return sum;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
    int num = NumberOf1(n);
	printf("%d ", num);
	return 0;
}