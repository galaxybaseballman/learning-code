#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int y = 0;
    int m = 0;
    int d = 0;
    int day = 0;
    int days = 0;
    scanf("%d%d%d", &y, &m, &day);


    while (--m)
    {
        switch (m)
        {
        case 1:
            d = 31;
            break;
        case 2:
            d = 28;
            break;
        case 3:
            d = 31;
            break;
        case 4:
            d = 30;
            break;
        case 5:
            d = 31;
            break;
        case 6:
            d = 30;
            break;
        case 7:
            d = 31;
            break;
        case 8:
            d = 31;
            break;
        case 9:
            d = 30;
            break;
        case 10:
            d = 31;
            break;
        case 11:
            d = 30;
            break;
        case 12:
            d = 31;
            break;
        }
        if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
        {
            if (m == 2)
            {
                d = 29;
            }
        }
        else
        {
            if (m == 2)
            {
                d = 28;
            }
        }
        days += d;
    }
    printf("%d", days + day);

    return 0;



}