#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
int main()
{
	int(*ptr)[5] = NULL;
	ptr = (int(*)[5])malloc(15 * sizeof(int));
	
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 5; j++)
			ptr[i][j] = i + j;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 5; j++)
			printf("%d ", ptr[i][j]);
		printf("\n");
	}
	return 0;
}