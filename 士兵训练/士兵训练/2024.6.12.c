#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

typedef struct SoldierTrain
{
	int val;
	struct SoldierTrain* next;
}SoldierT;

SoldierT* CreateSoldierT(int x)
{
	SoldierT* newst = (SoldierT*)malloc(sizeof(SoldierT));
	if (newst == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	newst->next = NULL;
	newst->val = x;
	return newst;
}

void SoldierTPush(SoldierT** ppst, int x)
{
	SoldierT* newnode = CreateSoldierT(x);

	if (*ppst == NULL)
	{
		*ppst = newnode;
		return;
	}
	SoldierT* pcur = *ppst;
	while (pcur->next)
		pcur = pcur->next;
	pcur->next = newnode;
}

void SoldierTPop(SoldierT* pst, int i, int* pn)
{
	SoldierT* pcur = pst;
	SoldierT* prev = pst;
	int ii = i;
	while (pcur)
	{
		while (--ii && pcur)
		{
			prev = pcur;
			pcur = pcur->next;
		}
		if (pcur)
		{
			prev->next = pcur->next;
			pcur = prev->next;
			(*pn)--;
		}
		ii = i;
	}
}

int main()
{
	SoldierT* soldt = NULL;

	printf("请输入有几个士兵：");
	int n = 0;
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		SoldierTPush(&soldt, i);
	int num = 1;
	while (n > 3)
	{
		if (num % 2 == 1)
			SoldierTPop(soldt, 2, &n);
		else
			SoldierTPop(soldt, 3, &n);
		num++;
	}

	while (soldt)
	{
		printf("%d ", soldt->val);
		soldt = soldt->next;
	}

	return 0;
}