#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

typedef struct Element
{
	int val;
	struct Element* next;
}Element;

int cmp(const void* p1, const void* p2)
{
	return *(int*)p1 - *(int*)p2;
}

Element* CreateElement(int x)
{
	Element* newnode = (Element*)malloc(sizeof(Element));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	newnode->next = NULL;
	newnode->val = x;
	return newnode;
}

void ElementPush(Element** pphead, Element** pptail, int x)
{
	Element* newelement = CreateElement(x);

	if (*pphead == NULL)
	{
		*pphead = *pptail = newelement;
		return;
	}
	(*pptail)->next = newelement;
	*pptail = newelement;
}

bool ElementEqual(Element* a, Element* b)//判等
{
	while (a && b)
	{
		if (a->val != b->val)
			return false;
		a = a->next;
		b = b->next;
	}
	if (a == NULL && b == NULL)
		return true;
	return false;
}
Element* ElementIntersection(Element* a, Element* b)//交集
{
	Element* newhead = (Element*)malloc(sizeof(Element));
	if (newhead == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	Element* pa = a;
	Element* pb = b;
	Element* pcur = newhead;

	while (pa && pb)
	{
		if (pa->val == pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			pb = pb->next;
			continue;
		}
		if (pa->val < pb->val)
		{
			pa = pa->next;
			continue;
		}
		if (pa->val > pb->val)
		{
			pb = pb->next;
			continue;
		}
	}
	pcur->next = NULL;

	return newhead->next;
}
Element* ElementUnion(Element* a, Element* b)//并集
{
	Element* newhead = (Element*)malloc(sizeof(Element));
	if (newhead == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	Element* pa = a;
	Element* pb = b;
	Element* pcur = newhead;

	while (pa && pb)
	{
		if (pa->val == pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			pb = pb->next;
			continue;
		}
		if (pa->val < pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			continue;
		}
		if (pa->val > pb->val)
		{
			pcur->next = pb;
			pcur = pb;
			pb = pb->next;
			continue;
		}
	}
	while (pa)
	{
		pcur->next = pa;
		pcur = pa;
		pa = pa->next;
	}
	while (pb)
	{
		pcur->next = pb;
		pcur = pb;
		pb = pb->next;
	}
	pcur->next = NULL;

	return newhead->next;
}
Element* ElementTranslation(Element* a, Element* b)//差集
{
	Element* newhead = (Element*)malloc(sizeof(Element));
	if (newhead == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	Element* pa = a;
	Element* pb = b;
	Element* pcur = newhead;

	while (pa && pb)
	{
		if (pa->val == pb->val)
		{
			pa = pa->next;
			pb = pb->next;
			continue;
		}
		if (pa->val < pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			continue;
		}
		if (pa->val > pb->val)
		{
			pb = pb->next;
		}
	}
	while (pa)
	{
		pcur->next = pa;
		pcur = pa;
		pa = pa->next;
	}
	pcur->next = NULL;

	return newhead->next;
}

void menu()
{
	printf("***********************\n");
	printf("*** 1.交集   2.并集 ***\n");
	printf("*** 3.差集   4.判等 ***\n");
	printf("***     0. 退出     ***\n");
	printf("***********************\n");
}
int main()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	menu();
	printf("请输入对应的操作:");
	scanf("%d", &input);
	while (input)
	{
		int numa, numb;
		printf("输入两个链表的个数：");
		scanf("%d %d", &numa, &numb);
		int a[100];
		int b[100];
		for (int i = 0; i < numa; i++)
			scanf("%d", &a[i]);
		for (int i = 0; i < numb; i++)
			scanf("%d", &b[i]);

		qsort(a, numa, sizeof(int), cmp);
		qsort(b, numb, sizeof(int), cmp);
		Element* pheada = NULL;
		Element* ptaila = NULL;
		for (int i = 0; i < numa; i++)
			ElementPush(&pheada, &ptaila, a[i]);

		Element* pheadb = NULL;
		Element* ptailb = NULL;
		for (int i = 0; i < numb; i++)
			ElementPush(&pheadb, &ptailb, b[i]);

		
		Element* newhead = NULL;
		switch (input)
		{
		case 1:
			newhead = ElementIntersection(pheada, pheadb);
			while (newhead)
			{
				printf("%d ", newhead->val);
				newhead = newhead->next;
			}
			printf("\n");
			break;
		case 2:
			newhead = ElementUnion(pheada, pheadb);
			while (newhead)
			{
				printf("%d ", newhead->val);
				newhead = newhead->next;
			}
			printf("\n");
			break;
		case 3:
			newhead = ElementTranslation(pheada, pheadb);
			while (newhead)
			{
				printf("%d ", newhead->val);
				newhead = newhead->next;
			}
			printf("\n");
			break;
		case 4:
			printf("%d\n", ElementEqual(pheada, pheadb));
			break;
		case 0:
			printf("退出程序\n");
			break;
		default:
			printf("请输入0~4的数\n");
			break;
		}
		menu();
		printf("请输入对应的操作:");
		scanf("%d", &input);
	}

	return 0;
}