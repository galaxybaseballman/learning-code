#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int m = 0;
    int a = 1;
    int b = 0;

    scanf("%d", &m);
    for (int i = 0; i < m; i++)
    {
        b = 2 * i;

        a = a + b;
    }
    for (int j = 0; j < m; j++)
    {
        printf("%d", a);
        if (j < (m - 1))
        {
            printf("+");
        }
        a = a + 2;
    }

    return 0;
}