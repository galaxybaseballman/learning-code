#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int HashSearch1(int ht[], int m, int k, int* p)	/*形参p传指针，返回位置*/
{
	int i, j, flag = 0;						/*flag=0表示散列表未满*/
	j = H(k);								/*计算散列地址*/
	i = j;									/*记载比较的起始位置*/
	while (ht[i] != 0 && flag == 0)
	{
		if (ht[i] == k) {						/*比较若干次查找成功*/
			*p = i;
			return 1;
		}
		else
			i = (i + 1) % m;					/*向后探测一个位置*/
		if (i == j)
			flag = 1;					/*表已满*/
	}
	if (flag == 1)
	{
		printf("溢出");
		return -1;
	}		/*表满，产生溢出*/
	else {								/*比较若干次查找不成功，插入*/
		ht[i] = k;
		*p = i;
		return 0;
	}
}
int main()
{
 
}