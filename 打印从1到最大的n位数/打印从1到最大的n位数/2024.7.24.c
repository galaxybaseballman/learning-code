#define _CRT_SECURE_NO_WARNINGS 1

int* printNumbers(int n, int* returnSize) {
    // write code here
    int size = 1;
    for (int i = 1; i <= n; i++)
    {
        size *= 10;
    }
    int* p = (int*)malloc(sizeof(int) * (size - 1));
    *returnSize = size - 1;
    if (n == 1)
    {
        int a = 0;
        for (int i = 1; i <= 9; i++)
        {
            a = i;
            p[a - 1] = i;
        }
    }
    if (n == 2)
    {
        int a = 0;
        for (int i = 1; i <= 99; i++)
        {
            a = i;
            p[a - 1] = i;
        }
    }
    if (n == 3)
    {
        int a = 0;
        for (int i = 1; i <= 999; i++)
        {
            a = i;
            p[a - 1] = i;
        }
    }
    if (n == 4)
    {
        int a = 0;
        for (int i = 1; i <= 9999; i++)
        {
            a = i;
            p[a - 1] = i;
        }
    }
    if (n == 5)
    {
        int a = 0;
        for (int i = 1; i <= 99999; i++)
        {
            a = i;
            p[a - 1] = i;
        }
    }


    return p;
}