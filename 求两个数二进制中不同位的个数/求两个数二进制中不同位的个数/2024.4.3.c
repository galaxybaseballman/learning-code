#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    int sum = 0;
    for (int i = 0; i < 32; i++)
    {
        if ((a & (1 << i)) != (b & (1 << i)))
            sum++;
    }
    printf("%d", sum);
    return 0;
}