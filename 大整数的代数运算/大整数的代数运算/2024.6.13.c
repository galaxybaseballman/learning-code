#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void menu()
{
	printf("**************************\n");
	printf("*** 1. Add      2. Sub ***\n");
	printf("*** 3. Mul      2. Div ***\n");
	printf("***       0. exit      ***\n");
	printf("**************************\n");
}

char* Add(char* a, char* b, size_t la, size_t lb)//倒着存储
{
	size_t max = (la > lb) ? la + 1 : lb + 1;
	char* retdao = (char*)malloc(sizeof(char) * max + 1);
	char* ret = (char*)malloc(sizeof(char) * max + 1);

	int numr = 0;
	int carry = 0;
	while (la && lb)
	{
		int add = (a[la - 1] - '0') + (b[lb - 1] - '0') + carry;
		carry = 0;
		if (add >= 10)
		{
			add -= 10;
			carry++;
		}
		retdao[numr++] = add + '0';
		la--;
		lb--;
	}
	while (la)
	{
		int add = a[la - 1] - '0' + carry;
		carry = 0;
		if (add >= 10)
		{
			add -= 10;
			carry++;
		}
		retdao[numr++] = add + '0';
		la--;
	}
	while (lb)
	{
		int add = b[lb - 1] - '0' + carry;
		carry = 0;
		if (add >= 10)
		{
			add -= 10;
			carry++;
		}
		retdao[numr++] = add + '0';
		lb--;
	}
	retdao[numr] = '\0';

	size_t lret = strlen(retdao);
	int i = 0;
	for (i = 0; i < lret; i++)
	{
		ret[i] = retdao[lret - 1 - i];
	}
	ret[i] = '\0';
	free(retdao);
	return ret;
}

char* Sub(char* a, char* b, size_t la, size_t lb)
{
	size_t maxi = (la > lb) ? la : lb;
	char* ret = (char*)malloc(sizeof(char) * (maxi + 1));
	if (ret == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	int numr = 0;
	char* max = a;
	char* min = b;
	size_t diff = la - lb;
	if (lb > la)
	{
		max = b;
		min = a;
		diff = lb - la;
	}
	if (la == lb)
	{
		for (int i = 0; i < la; i++)
		{
			if (b[i] > a[i])
			{
				max = b;
				min = a;
				break;
			}
			if (a[i] > b[i])
				break;
		}
	}//找出大数和小数，让大数 - 小数

	size_t lmax = strlen(max);
	size_t lmin = strlen(min);

	for (int i = 0; i < lmin; i++)
	{
		if (max[lmax - 1 - i] < min[lmin - 1 - i])
		{
			int da = max[lmax - 1 - i] - '0' + 10;
			int xiao = min[lmin - 1 - i] - '0';
			ret[lmax - 1 - i] = da - xiao + '0';
			max[lmax - i - 2] = max[lmax - i - 2] - 1;
		}
		else
			ret[lmax - 1 - i] = max[lmax - 1 - i] - min[lmin - 1 - i] + '0';
	}
	for (int i = lmin; i < lmax; i++)
		ret[lmax - i - 1] = max[lmax - i - 1];
	ret[lmax] = '\0';

	return ret;
}

char* Mul(char* a, char* b, size_t la, size_t lb)
{
	char* ret = (char*)malloc(sizeof(char) * (la + lb + 1));
	if (ret == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	memset(ret, '0', la + lb + 1);
	ret[la + lb] = '\0';

	int i = 0;
	while (i < la)
	{
		char* number = (char*)malloc(sizeof(char) * (lb + i + 1));
		if (number == NULL)
		{
			perror("malloc fail");
			exit(-1);
		}
		int n = a[la - 1 - i] - '0';//a中每一位数
		int nb = 0;//遍历b中的数
		int carry = 0;
		for (int j = 0; j < i; j++)
			number[j] = '0';
		while (nb < lb)
		{
			int accu = (b[lb - nb - 1] - '0') * n + carry;
			carry = accu / 10;
			int yu = accu % 10;
			number[nb + i] = yu + '0';
			nb++;
		}
		if (carry)
			number[nb + i] = carry + '0';
		else
			nb--;
		char* numberdao = (char*)malloc(sizeof(char) * (lb + i + 1));
		if (numberdao == NULL)
		{
			perror("malloc fail");
			exit(-1);
		}
		for (int j = nb + i; j >= 0; j--)
			numberdao[j] = number[nb + i - j];
		char* destory = ret;
		ret = Add(ret, numberdao, la + lb, nb + i + 1);
		free(destory);
		i++;
	}

	return ret;
}

char* Div(char* a, char* b, size_t la, size_t lb)
{
	char* max = a;
	char* min = b;
	if (lb > la)
	{
		max = b;
		min = a;
	}
	if (la == lb)
	{
		for (int i = 0; i < la; i++)
		{
			if (b[i] > a[i])
			{
				max = b;
				min = a;
				break;
			}
			if (a[i] > b[i])
				break;
		}
	}//找出大数和小数，让大数 - 小数

	size_t lmax = strlen(max);
	size_t lmin = strlen(min);

	char* divisor = (char*)malloc(sizeof(char) * (lmax + 1));
	if (divisor == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	char* ret = (char*)malloc(sizeof(char) * (la + 1));
	if (ret == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	size_t diff = lmax - lmin;
	int weinum = 0;
	char* quo = max;//被除数和商
	while (weinum <= diff)
	{
		int i = 0;
		for (i = 0; i < weinum; i++)
			divisor[i] = '0';
		for (i = weinum; i < lmin + weinum; i++)
		{
			divisor[i] = min[i - weinum];
		}
		for (i = lmin + weinum; i < lmax; i++)
		{
			divisor[i] = '0';
		}
		divisor[i] = '\0';
		int result = 0;
		int quobig = 1;
		while (quobig)
		{
			quobig = 1;//判断quo是否大于divisor
			for (int j = 0; j < lmax; j++)
			{
				if (quo[j] < divisor[j])
				{
					quobig = 0;
					break;
				}
				if (quo[j] > divisor[j])
				{
					quobig = 1;
					break;
				}
			}
			if (quobig)
			{
				quo = Sub(quo, divisor, lmax, lmax);
				result++;
			}
		}
		ret[weinum++] = result + '0';
	}
	ret[weinum] = '\0';

	return ret;
}
int main()
{
	int input = 0;
	do
	{
		char* ret = NULL;
		char inta[100];
		char intb[100];
		printf("请输入两个整数:\n");
		scanf("%s", inta);
		scanf("%s", intb);

		size_t lengtha = strlen(inta);
		size_t lengthb = strlen(intb);

		menu();
		printf("请输入要执行的操作: ");
		scanf("%d", &input);

		switch (input)
		{
		case 1:
			ret = Add(inta, intb, lengtha, lengthb);
			printf("%s\n", ret);
			break;
		case 2:
			ret = Sub(inta, intb, lengtha, lengthb);
			printf("%s\n", ret);
			break;
		case 3:
			ret = Mul(inta, intb, lengtha, lengthb);
			printf("%s\n", ret);
			break;
		case 4:
			ret = Div(inta, intb, lengtha, lengthb);
			printf("%s\n", ret);
			break;
		case 0:
			printf("退出程序\n");
			break;
		default:
			printf("请输入0到4\n");
			break;
		}
	} while (input);

	return 0;
}