#pragma once
#include<stdio.h>

size_t mystrlen(const char* str);
char* mystrcpy(char* destination, const char* source);
char* mystrcat(char* destination, const char* source);
int mystrcmp(const char* str1, const char* str2);