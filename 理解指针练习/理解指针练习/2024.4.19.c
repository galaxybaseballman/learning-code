#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//int main()
//{
//	int a = 10;
//	printf("%d\n", sizeof(a));//4 int 类型
//	printf("%d\n", sizeof a);//4 操作符
//	printf("%d\n", sizeof(int));//4 int
//
//
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	char arr1[3] = { 'a', 'b', 'c' };
//	char arr2[] = "abc";
//	printf("%d\n", strlen(arr1));//随机值 /0没有
//	printf("%d\n", strlen(arr2));//3 3个之后/0
//
//	printf("%d\n", sizeof(arr1));//3 有三个之母
//	printf("%d\n", sizeof(arr2));//4 三个字母加/0
//	return 0;
//}

//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p = (struct Test*)0x100000;
int main()
{
	//int a[] = { 1,2,3,4 };
	//printf("%d\n", sizeof(a));//16 int型整个数组的大小
	//printf("%d\n", sizeof(a + 0));//4/8 还是int型的第一个数组*
	//printf("%d\n", sizeof(*a));//4 数组名，第一个数组 = a[0]* 
	//printf("%d\n", sizeof(a + 1));//4/8 地址* 指向a[1]
	//printf("%d\n", sizeof(a[1]));//4 int型
	//printf("%d\n", sizeof(&a));//4/8 地址 指向整个a数组
	//printf("%d\n", sizeof(*&a));//16 相当于a
	//printf("%d\n", sizeof(&a + 1));//4/8 地址 指向下一个数组，跳过了数组a
	//printf("%d\n", sizeof(&a[0]));// 4/8 地址 指向a[0]
	//printf("%d\n", sizeof(&a[0] + 1));// 4/8 地址 指向a[1]

	//char arr[] = { 'a','b','c','d','e','f' };
	//printf("%d\n", sizeof(arr));//6  字节char*6 = 6
	//printf("%d\n", sizeof(arr + 0));//4/8 指针
	//printf("%d\n", sizeof(*arr));//1 char型
	//printf("%d\n", sizeof(arr[1]));//1 解引用
	//printf("%d\n", sizeof(&arr));//4/8 地址 指向整个数组arr
	//printf("%d\n", sizeof(&arr + 1));//4/8 地址 指向下一个数组，跳过整个数组
	//printf("%d\n", sizeof(&arr[0] + 1));//4/8 地址 指向arr[1]

	//char arr[] = { 'a','b','c','d','e','f','\0'};
	//printf("%d\n", strlen(arr));//随机值 没有\0
	//printf("%d\n", strlen(arr + 0));////随机值  没有\0*
	////printf("%d\n", strlen(*arr));//错误*
	////printf("%d\n", strlen(arr[1]));//错误*
	//printf("%d\n", strlen(&arr));////随机值  没有\0*
	//printf("%d\n", strlen(&arr + 1));////随机值  没有\0*
	//printf("%d\n", strlen(&arr[0] + 1));////随机值  没有\0*

	//char arr[] = "abcdef";
	//printf("%d\n", sizeof(arr));//7 有 \0 6+1*
	//printf("%d\n", sizeof(arr + 0));//4/8 地址 指向arr[0]
	//printf("%d\n", sizeof(*arr));//1 解引用 第一个char类型
	//printf("%d\n", sizeof(arr[1]));//1 解引用
	//printf("%d\n", sizeof(&arr));//4/8 地址 指向整个arr数组
	//printf("%d\n", sizeof(&arr + 1));//4/8 地址 指向下一个数组，跳过了arr数组
	//printf("%d\n", sizeof(&arr[0] + 1));// 4/8 地址 指向arr[1]

	//char arr[] = "abcdef";
	//printf("%d\n", strlen(arr));//6 有\0
	//printf("%d\n", strlen(arr + 0));// 6 有\0
	////printf("%d\n", strlen(*arr));//错误 解引用 第一个字符
	////printf("%d\n", strlen(arr[1]));//错误
	//printf("%d\n", strlen(&arr));//6 有\0
	//printf("%d\n", strlen(&arr + 1));//随机值 跳过这个数组
	//printf("%d\n", strlen(&arr[0] + 1));//5 跳过第一个字符

	//char* p = "abcdef";
	//printf("%d\n", sizeof(p));//4/8 地址 指向整个字符
	//printf("%d\n", sizeof(p + 1));//4/8 地址 指向字符b
	//printf("%d\n", sizeof(*p));//1 解引用 相当于第一个字符
	//printf("%d\n", sizeof(p[0]));//1 * 解引用  相当于第一个字符
	//printf("%d\n", sizeof(&p));// 4/8 地址 指向整个字符
	//printf("%d\n", sizeof(&p + 1));// 4/8 地址 指向下一个字符 跳过整个字符串
	//printf("%d\n", sizeof(&p[0] + 1));// 4/8 地址 指向字符b

	char* p = "abcdef";
	printf("%d\n", strlen(p));//6 有\0
	printf("%d\n", strlen(p + 1));// 5 相对于从第二个字符开始 *
	//printf("%d\n", strlen(*p));//错误  * 
	//printf("%d\n", strlen(p[0]));//错误  *
	printf("%d\n", strlen(&p));//随机值 注意运行时恰好是6（？ ？）
	printf("%d\n", strlen(&p + 1));// 随机值 取p 的地址 ，没有/0
	printf("%d\n", strlen(&p[0] + 1));//5 解引用 从第二个字符开始

	//int a[3][4] = { 0 };
	//printf("%d\n", sizeof(a));//48 3*4*4
	//printf("%d\n", sizeof(a[0][0]));//4 int型 就一个
	//printf("%d\n", sizeof(a[0]));//16 相对于第一行所有4*4
	//printf("%d\n", sizeof(a[0] + 1));//4/8 地址* 指向a[1]
	//printf("%d\n", sizeof(*(a[0] + 1)));//4  * 相当于a[0][1]
	//printf("%d\n", sizeof(a + 1));// 4/8 地址 指向a[0][1]
	//printf("%d\n", sizeof(*(a + 1)));//16  * 相当于a[1]
	//printf("%d\n", sizeof(&a[0] + 1));//4/8 地址 指向a[1]
	//printf("%d\n", sizeof(*(&a[0] + 1)));//16 *
	//printf("%d\n", sizeof(*a));////16 *相当于a[0]
	//printf("%d\n", sizeof(a[5]));// //16 *

	//int a[5] = { 1, 2, 3, 4, 5 };
	//int* ptr = (int*)(&a + 1);
	//printf("%d,%d", *(a + 1), *(ptr - 1)); 
	////2 相当于a[1]
	////5  * ptr-1了，退到了a[4]
	//return 0;

	/*printf("%p\n", p + 0x1);
	printf("%p\n", (unsigned long)p + 0x1);
	printf("%p\n", (unsigned int*)p + 0x1);
	return 0;*/

	//int a[3][2] = { (0, 1), (2, 3), (4, 5) };
	//int* p;
	//p = a[0];
	//printf("%d", p[0]);//1 * 逗号表达式
	//return 0;

	//int a[5][5];
	//int(*p)[4];//*（p+4）
	//p = a;
	//printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
	////* fffffffc -4
	// 虽然超出p的范围，但直接往后数就行，画个图
	//return 0;

	//int aa[2][5] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//int* ptr1 = (int*)(&aa + 1);
	//int* ptr2 = (int*)(*(aa + 1));
	//printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
	////10 跳过整个地址-1，刚好是10
	////5 * *(aa+1)=aa[1]=678910,*ptr2-1=ptr[0][4];
	//return 0;

	//char* a[] = { "work","at","alibaba" };
	//char** pa = a;
	//pa++;
	//printf("%s\n", *pa);//at *pa = a[0] pa++ = a[1]
	//return 0;

	//char* c[] = { "ENTER","NEW","POINT","FIRST" };
	//char** cp[] = { c + 3,c + 2,c + 1,c };
	//char*** cpp = cp;
	//printf("%s\n", **++cpp);//point ++ccp = cp[1] = c[2] = point 
	// 切记这里已经++cpp,下面的cpp已经被+1了
	//printf("%s\n", *-- * ++cpp + 3);//er* 这里*++cpp = cp[2]  = c+1; --c+1 = c;
	// 所以*-- = c[0] = e + 3 =往后推三个字母得到 er
	//printf("%s\n", *cpp[-2] + 3);//st* *cpp[-2] = cp[0] = c+3 = first
	// 所以*cpp[-2] = f;+3 =往后推三个字母得到st
	//printf("%s\n", cpp[-1][-1] + 1);//ew* cpp[-1][-1] = *(*(cpp-1)-1) = *(cp[1]-1) = *(c+2-1)=c[1] = new;
	// +1之后往后推三个字母得到ew;
	//return 0;

}

