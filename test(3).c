#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

typedef struct Element
{
	int val;
	struct Element* next;
}Element;

int cmp(const void* p1, const void* p2)
{
	return *(int*)p1 - *(int*)p2;
}

Element* CreateElement(int x)
{
	Element* newnode = (Element*)malloc(sizeof(Element));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	newnode->next = NULL;
	newnode->val = x;
	return newnode;
}

void ElementPush(Element** pphead, Element** pptail, int x)
{
	Element* newelement = CreateElement(x);

	if (*pphead == NULL)
	{
		*pphead = *pptail = newelement;
		return;
	}
	(*pptail)->next = newelement;
	*pptail = newelement;
}

bool ElementEqual(Element* a, Element* b)//判等
{
	while (a && b)
	{
		if (a->val != b->val)
			return false;
		a = a->next;
		b = b->next;
	}
	if(a == NULL && b == NULL)
		return true;
	return false;
}
Element* ElementIntersection(Element* a, Element* b)//交集
{
	Element* newhead = (Element*)malloc(sizeof(Element));
	if (newhead == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	Element* pa = a;
	Element* pb = b;
	Element* pcur = newhead;

	while (pa && pb)
	{
		if (pa->val == pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			pb = pb->next;
			continue;
		}
		if (pa->val < pb->val)
		{
			pa = pa->next;
			continue;
		}
		if (pa->val > pb->val)
		{
			pb = pb->next;
			continue;
		}
	}
	pcur->next = NULL;

	return newhead->next;
}
Element* ElementUnion(Element* a, Element* b)//并集
{
	Element* newhead = (Element*)malloc(sizeof(Element));
	if (newhead == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	Element* pa = a;
	Element* pb = b;
	Element* pcur = newhead;

	while (pa && pb)
	{
		if (pa->val == pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			pb = pb->next;
			continue;
		}
		if (pa->val < pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			continue;
		}
		if (pa->val > pb->val)
		{
			pcur->next = pb;
			pcur = pb;
			pb = pb->next;
			continue;
		}
	}
	while (pa)
	{
		pcur->next = pa;
		pcur = pa;
		pa = pa->next;
	}
	while (pb)
	{
		pcur->next = pb;
		pcur = pb;
		pb = pb->next;
	}
	pcur->next = NULL;

	return newhead->next;
}
Element* ElementTranslation(Element* a, Element* b)//差集
{
	Element* newhead = (Element*)malloc(sizeof(Element));
	if (newhead == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	Element* pa = a;
	Element* pb = b;
	Element* pcur = newhead;

	while (pa && pb)
	{
		if (pa->val == pb->val)
		{
			pa = pa->next;
			pb = pb->next;
			continue;
		}
		if (pa->val < pb->val)
		{
			pcur->next = pa;
			pcur = pa;
			pa = pa->next;
			continue;
		}
		if (pa->val > pb->val)
		{
			pb = pb->next;
		}
	}
	while (pa)
	{
		pcur->next = pa;
		pcur = pa;
		pa = pa->next;
	}
	pcur->next = NULL;

	return newhead->next;
}

int main()
{
	srand((unsigned int)time(NULL));
	int numa, numb;
	printf("输入两个链表的个数：");
	scanf("%d %d", &numa, &numb);
	int a[100];
	int b[100];
	for (int i = 0; i < numa; i++)
		scanf("%d", &a[i]);
	for (int i = 0; i < numb; i++)
		scanf("%d", &b[i]);

	qsort(a, numa, sizeof(int), cmp);
	qsort(b, numb, sizeof(int), cmp);
	Element* pheada = NULL;
	Element* ptaila = NULL;
	for (int i = 0; i < numa; i++)
		ElementPush(&pheada, &ptaila, a[i]);

	Element* pheadb = NULL;
	Element* ptailb = NULL;
	for (int i = 0; i < numb; i++)
		ElementPush(&pheadb, &ptailb, b[i]);

	printf("%d\n",ElementEqual(pheada, pheadb));
	
	Element* newhead1 = ElementIntersection(pheada, pheadb);
	Element* newhead2 = ElementUnion(pheada, pheadb);
	Element* newhead3 = ElementTranslation(pheada, pheadb);
	while (newhead1)
	{
		printf("%d ", newhead1->val);
		newhead1 = newhead1->next;
	}
	while (newhead2)
	{
		printf("%d ", newhead2->val);
		newhead2 = newhead2->next;
	}
	while (newhead3)
	{
		printf("%d ", newhead3->val);
		newhead3 = newhead3->next;
	}

	return 0;
}