#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>

int NumberPyramid(int arr[][10], int layer)
{
	int max = 0;
	int maxn = 0;
	for (int i = 0; i < layer; i++)
	{
		maxn = arr[i][0];
		for (int j = 0; j <= i; j++)
			if (max < arr[i][j])
				maxn = arr[i][j];
		max += maxn;
	}
	return max;
}

int main()
{
	int layer = 0;
	printf("请输入数塔层数：");
	scanf("%d", &layer);

	int arr[10][10] = { 0 };
	printf("请输入数据:\n");
	for (int i = 0; i < layer; i++)
		for (int j = 0; j <= i; j++)
			scanf("%d", &arr[i][j]);

	printf("max = %d\n", NumberPyramid(arr, layer));
	return 0;
}