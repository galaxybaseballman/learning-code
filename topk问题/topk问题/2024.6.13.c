#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<time.h>
#include<assert.h>

void CreateNum()
{
	FILE* pf = fopen("test.txt", "w");
	if (pf == NULL)
	{
		perror("fopen fail");
		exit(-1);
	}
	srand((unsigned int)time(NULL));

	int size = 100000;
	for (int i = 0; i < size; i++)
	{
		fprintf(pf, "%d\n", rand());
	}
	fclose(pf);
	pf = NULL;
}

void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void AdjustDown(int* arr, int size, int parent)
{
	assert(arr);

	int child = parent * 2 + 1;
	while (child < size)
	{
		if ((child + 1) < size && arr[child] > arr[child + 1])
		{
			child++;
		}

		if (arr[parent] > arr[child])
		{
			Swap(&arr[parent], &arr[child]);
		}
		parent = child;
		child = parent * 2 + 1;
	}
}

void Top()
{
	int k = 0;
	printf("要找前k的数据，k的值：");
	scanf("%d", &k);

	FILE* pf = fopen("test.txt", "r");
	//创建k大小的数组
	int* arr = (int*)malloc(sizeof(int) * k);
	if (arr == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	//读取k个数据
	for (int i = 0; i < k; i++)
	{
		fscanf(pf, "%d", &arr[i]);
	}
	//建小堆
	int end = k - 1;
	while (end)
	{
		Swap(&arr[0], &arr[end]);
		AdjustDown(arr, end, 0);
		end--;
	}

	//读取剩余数据
	int ret = 0;
	int number = 0;
	while ((ret = fscanf(pf, "%d", &number)) != EOF)
	{
		if (number > arr[0])
		{
			arr[0] = number;
			AdjustDown(arr, k, 0);
		}
	}

	int sum = 0;
	for (int i = 0; i < k; i++)
	{
		printf("%d\n", arr[i]);
		sum += arr[i];
	}
	printf("sum = %d\n", sum);
}

int main()
{
	CreateNum();
	Top();
	return 0;
}