#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <malloc.h>

using namespace std;
/*将二叉链表的结点结构定义和各个函数定义放到这里*/
typedef char DataType;
typedef struct BiNode {
	DataType data;
	struct BiNode* lchild, * rchild;
} BiNode;
BiNode* root;
BiNode* CreatBiTree(BiNode* root) {
	char ch;
	cin >> ch; /*输入结点的数据信息*/
	if (ch == '#')
		root = NULL; /*递归结束，建立一棵空树*/
	else {
		root = (BiNode*)malloc(sizeof(BiNode)); /*生成新结点*/
		root->data = ch;						 /*新结点的数据域为ch*/
		root->lchild = CreatBiTree(root->lchild); /*递归建立左子树*/
		root->rchild = CreatBiTree(root->rchild); /*递归建立右子树*/
	}
	return root;
}
void PreOrder(BiNode *root) { //前序遍历
	if (root == NULL)
		return; /*递归调用的结束条件*/
	else {
		//根左右
		printf("%c ", root->data); /*访问根结点的数据域，为char型*/
		PreOrder(root->lchild);	   /*前序递归遍历root的左子树*/
		PreOrder(root->rchild);	   /*前序递归遍历root的右子树*/
	}
}
void InOrder(BiNode* root) {
	if (root == NULL)
		return;
	else {
		InOrder(root->lchild);
		printf("%c ", root->data);
		InOrder(root->rchild);
	}
}
void PostOrder(BiNode* root) {
	if (root == NULL)
		return;
	else {
		PostOrder(root->lchild);
		PostOrder(root->rchild);
		printf("%c ",root->data);
	}
}
int main() {
	BiNode* root = NULL;	  /*定义二叉树的根指针变量*/
	root = CreatBiTree(root); /*建立一棵二叉树*/
	printf("该二叉树的根结点是：%c\n", root->data);

	printf("\n该二叉树的前序遍历序列是：");
	PreOrder(root);

		printf("\n该二叉树的中序遍历序列是：");
		InOrder(root);

    	printf("\n该二叉树的后序遍历序列是：");
    	PostOrder(root);
	return 0;
}