#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int DigitSum(int n)
{
	if (n)
	{
		int m = n % 10 ;
		return  m + DigitSum(n/10);
	}
	else
		return 0;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int sum = DigitSum(n);
	printf("%d", sum);
	return 0;
}