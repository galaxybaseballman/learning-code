#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int a = 0, b = 0, c = 0;
    while (scanf("%d%d%d", &a, &b, &c) == 3)
    {
        if (a == b && b == c && a == c)
            printf("Equilateral triangle!\n");
        if ((a == b || a == c || b == c) && (a != b || a != c || b != c) && ((a + b) > c && (a + c) > b && (b + c) > a))
            printf("Isosceles triangle!\n");
        if ((a != b && a != c && b != c) && ((a + b) > c && (a + c) > b && (b + c) > a))
            printf("Ordinary triangle!\n");
        if ((a + b) <= c || (a + c) <= b || (b + c) <= a)
            printf("Not a triangle!\n");
    }
    return 0;
}