#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void year(int y)
{
	if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)
	{
		printf("是闰年");
	}
	else
	{
		printf("不是闰年");
	}
}
int main()
{
	int y = 0;
	scanf("%d", &y);
	year(y);
	return 0;
}