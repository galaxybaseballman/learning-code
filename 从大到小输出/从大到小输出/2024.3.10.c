#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int a = 0, b = 0, c = 0;
	scanf("%d%d%d", &a, &b, &c);
	int max;
	if (a < b)
	{
		max = a;
		a = b;
		b = max;
	}
	if (a < c)
	{
		max = a;
		a = c;
		c = max;
	}
	if (b < c)
	{
		max = b;
		b = c;
		c = max;
	}
	printf("%d %d %d", a, b, c);

	return 0;
	
}