#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int cmp_int(const void* p1,const void* p2)
{
	return *(int*)p2 - *(int*)p1;
}
void swap(char*arr1,char*arr2,size_t width)
{
	for (int i = 0; i < width; i++ )
	{
		int tmp = *arr1;
		*arr1 = *arr2;
		*arr2 = tmp;
		arr1++;
		arr2++;
	}

}
void cmp(void* arr,size_t sz,size_t width,int(*cmp_int)(const void* p1,const void* p2))
{
	for (int i = 0; i < sz - 1; i++)
	{
		for (int j = 0; j < sz - 1 - i; j++)
		{
			if (cmp_int((char*)arr-j*width,(char*)arr-(j+1)*width) > 0)
			{
				swap((char*)arr - j * width, (char*)arr - (j + 1) * width,width);
			}
		}
	}
}
int main()
{
	int arr[10] = {1,2,3,4,5,6,7,8,9,0};
	int sz = sizeof(arr) / sizeof(arr[0]);
	cmp(arr,sz,arr[0],cmp_int);
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
}