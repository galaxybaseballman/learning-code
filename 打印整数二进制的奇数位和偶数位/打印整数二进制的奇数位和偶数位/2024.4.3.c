#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int a = 0;
	scanf("%d", &a);
	for (int i = 0; i < 32; i++)
	{
		if (i % 2 == 0)
		{
			if (a & (1 << i))
			{
				printf("1");
			}
			else
				printf("0");
		}
		
	}
	printf("\n");
	for (int i = 0; i < 32; i++)
	{
		if (i % 2 != 0)
		{
			if (a & (1 << i))
			{
				printf("1");
			}
			else
				printf("0");
		}

	}
	return 0;
}