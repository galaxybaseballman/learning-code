#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void count(int a)
{
	if (a > 9)
	{
		count(a / 10);
		printf("%d\n", a % 10);
	}
	else
		printf("%d\n", a);
}
int main()
{
	int a = 0;
	scanf("%d", &a);
	count(a);
	return 0;
}