#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int bin_search(int arr[], int left, int right, int key)
{
	int mid = (left + right) / 2;
	if (arr[mid] == key)
	{
		return mid;
	}
	else
	{
		while (arr[mid] != key)
		{
			if (right > left)
			{
				if (key > arr[mid])
				{
					left = mid + 1;
					mid = (left + right) / 2;
				}
				if (key < arr[mid])
				{
					right = mid - 1;
					mid = (right + left) / 2;
				}
			}
			else
			return -1;
		}
	}
}
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int left = 0;
	int right = sizeof(arr)/sizeof(arr[0])-1;
	int key = 0;
	scanf("%d", &key);
	int sun = bin_search(arr, left, right, key);
	printf("%d\n", sun);
	
	return 0;
}