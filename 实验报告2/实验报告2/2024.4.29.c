#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
/*将单链表的结点结构定义和链栈的各个函数定义放到这里*/
typedef int DataType;          /*栈元素的数据类型，假设为int型*/
typedef struct Node
{
    DataType data;				/*存放栈元素的数据域*/
    struct Node* next;  			/*存放下一个结点的地址*/
} Node;
Node* top;					/*栈顶指针*/
void InitStack()
{
	top = NULL;
}

void Push(DataType x)
{
	Node* s = (Node*)malloc(sizeof(Node));          /*申请一个结点s*/
	s->data = x;
	s->next = top;
	top = s;                          /*将结点s插在栈顶*/
}
int GetTop(DataType* ptr)
{
	if (top == NULL) { printf("下溢错误，取栈顶失败\n"); return 0; }
	*ptr = top->data; return 1;
}

int Pop(DataType* ptr)
{
	Node* p = top;
	if (top == NULL) { printf("下溢错误，删除失败\n"); return 0; }
	*ptr = top->data;                              /*存储栈顶元素*/
	top = top->next;                               /*将栈顶结点摘链*/
	free(p);
	return 1;
}
int Empty()
{
	if (top == NULL) return 1;                     /*栈空则返回1*/
	else return 0;
}

void printlist()
{
	Node* p = top;
	printf("当前链栈为：");
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
}
//20231395-崔铭学
int main()
{
	DataType x;
	InitStack();                                 /*初始化链栈*/
	printf("对2，4，6，8执行入栈操作，\n");
	Push(2);
	Push(4);
	Push(6);
	Push(8);
	printlist();
	if (GetTop(&x) == 1)
		printf("\n当前栈顶元素为：%d\n\n", x);            /*输出当前栈顶元素10*/

	if (Pop(&x) == 1)
		printf("执行一次出栈操作，删除元素：%d\n", x);    /*输出出栈元素10*/
	printlist();
	if (GetTop(&x) == 1)
		printf("\n当前栈顶元素为：%d\n\n", x);           /*输出当前栈顶元素15*/

	printf("请输入待插入元素：");
	scanf("%d", &x);
	Push(x);
	if (Empty() == 1)
		printf("栈为空\n");
	else
	{
		printf("栈非空!!\n");                  /*栈有2个元素，输出栈非空*/
		printlist();
		GetTop(&x);
		printf("\n当前栈顶元素为：%d\n\n", x);           /*输出当前栈顶元素15*/
	}
	//	DestroyStack(top);
	return 0;
}
