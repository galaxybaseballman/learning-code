#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int a, b, c, d;

    while (scanf("%d%d", &a, &b) == 2) {

        c = a / b;
        d = a % b;
        printf("%d %d", c, d);
    }
    return 0;
}