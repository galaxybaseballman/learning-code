#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int i = 0;
	int z = 0;
	scanf("%d", &z);
	for (i = 1; i < 100000; i++)
	{
		int a = i % 10;
		int b = i / 10 % 10;
		int c = i / 100 % 10;
		int d = i / 1000 % 10;
		int e = i / 10000 % 10;
		int sum = a + b * 10 + c * 100 + d * 1000 + e * 10000;
		if (i < 10)
		{
			if (sum == a)
			{
				printf("%d ", sum);
			}
		}
		if (i > 10 && i < 100)
		{
			if (sum == a * a + b * b)
			{
				printf("%d ", sum);
			}
		}
		if (i > 100 && i < 1000)
		{
			if (sum == a * a * a + b * b * b + c * c * c)
			{
				printf("%d ", sum);
			}
		}
		if (i > 1000 && i < 10000)
		{
			if (sum == a * a * a * a + b * b * b * b + c * c * c * c + d * d * d * d)
			{
				printf("%d ", sum);
			}
		}
		if (i > 10000 && i < 100000)
		{
			if (sum == a * a * a * a * a + b * b * b * b * b + c * c * c * c * c + d * d * d * d * d + e * e * e * e * e)
			{
				printf("%d ", sum);
			}
		}
	}
		
	return 0;
}