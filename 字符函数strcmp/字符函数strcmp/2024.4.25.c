#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<assert.h>
int my_strcmp(const char* arr, const char* arr1)
{
	assert(arr && arr1);
	while (*arr == *arr1)
	{
		arr++;
		arr1++;
		if (*arr == '\0')
		{
			break;
		}
	}
	return *arr - *arr1;
}
int main()
{ 
	char arr[20] = "cui";
	char arr1[20] = "mui";
	int ret = my_strcmp(arr, arr1);
	printf("%d", ret);
	return 0;
}