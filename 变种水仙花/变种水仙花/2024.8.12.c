#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main() {
    int i = 10000;
    int a = 0;
    for (i = 10000; i < 100000; i++)
    {
        if (i == (i / 10) * (i % 10) + (i / 100) * (i % 100) + (i / 1000) * (i % 1000) + (i / 10000) * (i % 10000))
        {
            printf("%d ", i);
        }
    }
    return 0;
}